package main

import model "zapirus/internal/models"

//go:generate swagger generate spec -o swagrreq.json --scan-models

// swagger:route POST /search vacancyNewListRequest
// Запись в базу данных вакансий с ХабрКарьера по ключевому слову.
// responses:
//	200: vacancyNewListResponse

type search struct {
	Query string `json:"query"`
}

// swagger:parameters vacancyNewListRequest
type vacancyNewListRequest struct {
	// Поисковый запрос.
	// Required: true
	// In: body
	Query search
}

// swagger:response vacancyNewListResponse
type vacancyNewListResponse struct {
}

// swagger:route POST /delete vacancyDeleteRequest
// Удалить вакансию из базы данных по её ID.
// responses:
//	200: vacancyDeleteResponse

type id struct {
	ID string `json:"id"`
}

// swagger:parameters vacancyDeleteRequest
type vacancyDeleteRequest struct {
	// ID вакансии.
	// Required: true
	// In: body
	ID id
}

// swagger:response vacancyDeleteResponse
type vacancyDeleteResponse struct {
}

// swagger:route POST /get vacancyGetByIDRequest
// Получить вакансию по её ID.
// responses:
//	200: vacancyGetByIDResponse

// swagger:parameters vacancyGetByIDRequest
type vacancyGetByIDRequest struct {
	// ID вакансии.
	// Required: true
	// In: body
	ID id
}

// swagger:response vacancyGetByIDResponse
type vacancyGetByIDResponse struct {
	// in: body
	Body model.Vacancy
}

// swagger:route GET /list vacancyGetListRequest
// Получить все вакансии из БД.
// responses:
//	200: vacancyGetListResponse

// swagger:response vacancyGetListResponse
type vacancyGetListResponse struct {
	// in: body
	Body []model.Vacancy
}

