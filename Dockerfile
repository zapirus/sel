#build stage
FROM golang:1.20-alpine AS builder

WORKDIR /parser
COPY . .
RUN go build -o app cmd/main.go

#run stage
FROM alpine
WORKDIR /parser
COPY --from=builder /parser/app .
COPY /public/swagger.json ./public/
COPY /config.yml .

EXPOSE 8000
CMD ./app