package transport

import (
	"encoding/json"
	"net/http"

	"zapirus/internal/model"
)

type Handler struct {
	srv Service
}

func NewHandlers(s Service) *Handler {
	return &Handler{srv: s}
}

type Service interface {
	VacancyCreate(q string) error
	VacancyGetByID(id string) (model.Vacancy, error)
	VacancyGetList() ([]model.Vacancy, error)
	VacancyDelete(id string) error
}

func (h Handler) SearchVacancy(w http.ResponseWriter, r *http.Request) {
	var body map[string]string
	err := json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	q, ok := body["query"]
	if !ok {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if q == "" {
		http.Error(w, "empty request", http.StatusBadRequest)
		return
	}
	err = h.srv.VacancyCreate(q)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (h Handler) DeleteVacancy(w http.ResponseWriter, r *http.Request) {
	var body map[string]string
	err := json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	rawID, ok := body["id"]
	if !ok {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = h.srv.VacancyDelete(rawID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (h Handler) GetVacancy(w http.ResponseWriter, r *http.Request) {
	var body map[string]string
	err := json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	rawID, ok := body["id"]
	if !ok {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	res, err := h.srv.VacancyGetByID(rawID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(res)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (h Handler) ListVacancy(w http.ResponseWriter, r *http.Request) {
	res, err := h.srv.VacancyGetList()
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(res)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
