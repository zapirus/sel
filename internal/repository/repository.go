package repository

import (
	"fmt"
	"strconv"
	"sync"

	"zapirus/internal/model"
)

type VacancyStorage struct {
	vacancy map[int]model.Vacancy
	sync.RWMutex
}

func NewRepository() *VacancyStorage {
	return &VacancyStorage{
		vacancy: make(map[int]model.Vacancy),
	}
}

func (v *VacancyStorage) Create(dto model.Vacancy) error {
	v.Lock()
	defer v.Unlock()
	rawID, err := strconv.Atoi(dto.Identifier.Value)
	if err != nil {
		return err
	}

	v.vacancy[rawID] = dto
	return nil
}

func (v *VacancyStorage) GetByID(id int) (model.Vacancy, error) {
	if v, ok := v.vacancy[id]; ok {
		return v, nil
	}
	return model.Vacancy{}, fmt.Errorf("vacancy id %d not found", id)
}

func (v *VacancyStorage) GetList() ([]model.Vacancy, error) {
	if len(v.vacancy) == 0 {
		return nil, fmt.Errorf("vacancy not found")
	}
	res := make([]model.Vacancy, len(v.vacancy))
	i := 0
	for _, v := range v.vacancy {
		res[i] = v
		i++
	}

	return res, nil
}

func (v *VacancyStorage) Delete(id int) error {
	if _, ok := v.vacancy[id]; !ok {
		return fmt.Errorf("vacancy id %d not found", id)
	}
	delete(v.vacancy, id)
	return nil
}
