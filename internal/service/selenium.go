package service

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/PuerkitoBio/goquery"

	"zapirus/internal/model"

	//"github.com/PuerkitoBio/goquery"
	"github.com/tebeka/selenium"
	//"net/http"
)

var defaulturl string = "https://career.habr.com"

func (s *Service) SeleniumResponse(q string, sel selenium.WebDriver) error {
	// сразу обращаемся к странице с поиском вакансии по запросу
	page := 1 // номер страницы
	err := sel.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", page, q))
	if err != nil {
		return err
	}

	elem, err := sel.FindElement(selenium.ByCSSSelector, ".search-total")
	if err != nil {
		return err
	}
	vacancyCountRaw, err := elem.Text()
	temp := strings.Split(vacancyCountRaw, " ")
	vacancyCount, err := strconv.Atoi(temp[1])
	if err != nil {
		return err
	}
	var links []string
	for i := 0; i < vacancyCount; i += 25 {
		err = sel.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", page, q))
		if err != nil {
			return err
		}
		elems, err := sel.FindElements(selenium.ByCSSSelector, ".vacancy-card__title-link")
		if err != nil {
			return err
		}

		for j := range elems {
			var link string
			link, err = elems[j].GetAttribute("href")
			if err != nil {
				continue
			}
			links = append(links, defaulturl+link)
		}
		page++
	}

	for _, v := range links {
		resp, err := http.Get(v)
		if err != nil {
			return err
		}
		var doc *goquery.Document
		doc, err = goquery.NewDocumentFromReader(resp.Body)
		if err != nil && doc != nil {
			if err != nil {
				return err
			}
		}
		dd := doc.Find("script[type=\"application/ld+json\"]")
		if dd == nil {
			return nil
		}
		var repo model.Vacancy
		err = json.Unmarshal([]byte(dd.Text()), &repo)

		if err != nil {
			continue
		}
		err = s.repo.Create(repo)
		if err != nil {
			return err
		}

	}
	return nil
}
