package service

import (
	"strconv"

	"zapirus/internal/model"
	"zapirus/pkg/seleniumdriver"
)

type Service struct {
	repo Repository
}

func NewService(r Repository) *Service {
	return &Service{repo: r}
}

type Repository interface {
	Create(dto model.Vacancy) error
	GetByID(id int) (model.Vacancy, error)
	GetList() ([]model.Vacancy, error)
	Delete(id int) error
}

func (s *Service) VacancyCreate(q string) error {
	sel := seleniumdriver.NewSeleniumService()
	// после окончания программы завершаем работу драйвера
	defer sel.Quit()
	err := s.SeleniumResponse(q, sel)
	if err != nil {
		return err
	}
	return nil
}

func (s *Service) VacancyGetByID(id string) (model.Vacancy, error) {
	rawID, err := strconv.Atoi(id)
	if err != nil {
		return model.Vacancy{}, err
	}
	res, err := s.repo.GetByID(rawID)
	if err != nil {
		return model.Vacancy{}, err
	}
	return res, nil
}

func (s *Service) VacancyGetList() ([]model.Vacancy, error) {
	res, err := s.repo.GetList()
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (s *Service) VacancyDelete(id string) error {
	rawID, err := strconv.Atoi(id)
	if err != nil {
		return err
	}
	res := s.repo.Delete(rawID)
	return res
}
