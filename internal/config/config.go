package config

import (
	"log"

	"github.com/ilyakaznacheev/cleanenv"
)

type Config struct {
	ModeWork struct {
		IsDebug string `yaml:"isDebug" env-description:"Debug mode" env-default:"yes"`
	} `yaml:"workMode"`
	Server struct {
		HostPort string `yaml:"host_port" env-description:"Server host" env-default:"localhost:8000"`
	} `yaml:"server"`
}

var path = "config.yml"

func NewConfig() Config {

	log.Println("\t\tRead application configuration...")
	var cfg Config

	if err := cleanenv.ReadConfig(path, &cfg); err != nil {
		help, _ := cleanenv.GetDescription(&cfg, nil)
		log.Println(help)
		log.Fatalf("%s", err)
	}
	log.Println("\t\tGet configuration - OK!")

	return cfg
}
