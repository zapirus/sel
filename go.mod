module gitlab.com/zapirus/selenium

go 1.20

require (
	github.com/PuerkitoBio/goquery v1.8.1
	github.com/go-chi/chi v1.5.4
	github.com/ilyakaznacheev/cleanenv v1.4.2
	github.com/tebeka/selenium v0.9.9
	go.uber.org/zap v1.24.0
)

require (
	github.com/BurntSushi/toml v1.1.0 // indirect
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/blang/semver v3.5.1+incompatible // indirect
	github.com/joho/godotenv v1.4.0 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	golang.org/x/net v0.7.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
	olympos.io/encoding/edn v0.0.0-20201019073823-d3554ca0b0a3 // indirect
)
