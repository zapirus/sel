package seleniumdriver

import (
	"log"

	"github.com/tebeka/selenium"
)

const maxTries = 5

func NewSeleniumService() selenium.WebDriver {
	// прописываем конфигурацию для драйвера
	/*caps := selenium.Capabilities{
		"browserName": "chrome",
	}

	// добавляем в конфигурацию драйвера настройки для chrome
	chrCaps := chrome.Capabilities{
		W3C: true,
	}
	caps.AddChrome(chrCaps)*/
	caps := selenium.Capabilities{"browserName": "firefox"}

	// переменная нашего веб драйвера
	var wd selenium.WebDriver
	var err error
	// прописываем адрес нашего драйвера
	//urlPrefix := selenium.DefaultURLPrefix
	// немного костылей чтобы драйвер не падал
	i := 1
	for i < maxTries {
		wd, err = selenium.NewRemote(caps, "http://seleniumFirefox:4444/wd/hub")
		if err != nil {
			log.Println(err)
			i++
			continue
		}
		break
	}

	// ждем 60 секунд, чтобы успеть посмотреть результат
	//time.Sleep(60 * time.Second)

	return wd
}
