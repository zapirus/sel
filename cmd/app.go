package cmd

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"

	"zapirus/api"
	"zapirus/internal/config"
	"zapirus/internal/repository"
	"zapirus/internal/service"
	"zapirus/internal/transport"
	"zapirus/pkg/graceful"
	"zapirus/pkg/logger"
)

func Run() {
	cfg := config.NewConfig()
	logger.Init(cfg.ModeWork.IsDebug)

	logger.Debug("Create router...")
	r := chi.NewRouter()
	r.Use(middleware.Logger)

	repo := repository.NewRepository()
	srv := service.NewService(repo)
	controller := transport.NewHandlers(srv)

	Register(r, controller)
	graceful.StartServer(r, cfg.Server.HostPort)

}
func Register(r *chi.Mux, h *transport.Handler) {

	r.Post("/search", h.SearchVacancy)
	r.Post("/delete", h.DeleteVacancy)
	r.Post("/get", h.GetVacancy)
	r.Get("/list", h.ListVacancy)
	//SwaggerUI
	r.Get("/swagger", api.SwaggerUI)
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("./public"))).ServeHTTP(w, r)
	})
}
